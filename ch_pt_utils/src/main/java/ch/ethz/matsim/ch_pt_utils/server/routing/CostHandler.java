package ch.ethz.matsim.ch_pt_utils.server.routing;

import ch.ethz.matsim.baseline_scenario.transit.routing.EnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.EnrichedTransitRouter;
import ch.ethz.matsim.ch_pt_utils.ScheduleUtils;
import ch.ethz.matsim.ch_pt_utils.cost.solver.Ticket;
import ch.ethz.matsim.ch_pt_utils.cost.solver.TicketSolver;
import ch.ethz.matsim.ch_pt_utils.cost.stages.TransitStage;
import ch.ethz.matsim.ch_pt_utils.cost.stages.TransitStageTransformer;
import ch.ethz.matsim.ch_pt_utils.cost.tickets.TicketGenerator;
import ch.ethz.matsim.ch_pt_utils.server.routing.request.PlanRequest;
import ch.ethz.matsim.ch_pt_utils.server.routing.request.TripRequest;
import ch.ethz.matsim.ch_pt_utils.server.routing.response.*;
import io.javalin.Context;
import io.javalin.Handler;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.referencing.CRS;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.contrib.util.distance.DistanceCalculator;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.core.utils.geometry.GeometryUtils;
import org.matsim.facilities.Facility;
import org.matsim.pt.transitSchedule.api.*;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.NoninvertibleTransformException;
import org.opengis.referencing.operation.TransformException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

public class CostHandler implements Handler  {

    private final MathTransform transform;
    private final TransitSchedule schedule;
    private final TicketGenerator ticketGenerator;
    private final QuadTree<Long> stopQuadTree;

    public CostHandler(TransitSchedule schedule,
                          TicketGenerator ticketGenerator, TransitStageTransformer transformer, CoordinateReferenceSystem scheduleCRS)
            throws NoSuchAuthorityCodeException, FactoryException, NoninvertibleTransformException {
        this.transform = CRS.findMathTransform(CRS.decode("EPSG:4326"), scheduleCRS);
        this.schedule = schedule;
        this.ticketGenerator = ticketGenerator;

        Map<Id<TransitStopFacility>, Coord> stopCoordsMap =
                this.schedule.getFacilities().entrySet().stream().collect(
                        Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getCoord())
                );

        Double minX = stopCoordsMap.values().stream().mapToDouble(Coord::getX).min().getAsDouble();
        Double maxX = stopCoordsMap.values().stream().mapToDouble(Coord::getX).max().getAsDouble();
        Double minY = stopCoordsMap.values().stream().mapToDouble(Coord::getY).min().getAsDouble();
        Double maxY = stopCoordsMap.values().stream().mapToDouble(Coord::getY).max().getAsDouble();

        stopQuadTree = new QuadTree<>(minX, minY, maxX, maxX );
        stopCoordsMap.forEach((key, value) -> stopQuadTree.put(value.getX(), value.getY(),
            Long.parseLong(key.toString().split("\\.")[0])
        ));

    }

    @Override
    public synchronized void handle(Context ctx) throws Exception {
        try {
            PlanRequest planRequest = ctx.bodyAsClass(PlanRequest.class);
            PlanResponse planResponse = new PlanResponse();

            boolean isHalfFare = planRequest.isHalfFare;
            planResponse.isHalfFare = isHalfFare;

            List<TransitStage> transitStages = new LinkedList<>();

            for (TripRequest tripRequest : planRequest.trips) {
                DirectPosition originWGS84 = new DirectPosition2D(tripRequest.originLatitude,
                        tripRequest.originLongitude);
                DirectPosition destinationWGS84 = new DirectPosition2D(tripRequest.destinationLatitude,
                        tripRequest.destinationLongitude);

                DirectPosition originCoordinate = new DirectPosition2D();
                DirectPosition destinationCoordinate = new DirectPosition2D();

                transform.transform(originWGS84, originCoordinate);
                transform.transform(destinationWGS84, destinationCoordinate);

                Coord originCoord = new Coord(originCoordinate.getCoordinate()[0], originCoordinate.getCoordinate()[1]);
                Coord destinationCoord = new Coord(destinationCoordinate.getCoordinate()[0],
                        destinationCoordinate.getCoordinate()[1]);

                double departureTime = tripRequest.departureTime;

                long start_hafas_id = stopQuadTree.getClosest(originCoord.getX(), originCoord.getY());
                long end_hafas_id = stopQuadTree.getClosest(destinationCoord.getX(), destinationCoord.getY());
                List<Long> hafas_ids = Arrays.asList(start_hafas_id, end_hafas_id);
                double distance = CoordUtils.calcEuclideanDistance(originCoord, destinationCoord);

                TransitStage transitStage = new TransitStage(hafas_ids, distance, departureTime, -1, "pt");
                transitStages.add(transitStage);
            }

            if (transitStages.size() > 0) {
                Collection<Ticket> tickets = ticketGenerator.createTickets(transitStages, isHalfFare);
                TicketSolver.Result result = new TicketSolver().solve(transitStages.size(), tickets);

                for (Ticket ticket : result.tickets) {
                    TicketResponse ticketResponse = new TicketResponse();

                    ticketResponse.description = ticket.getDescription();
                    ticketResponse.price = ticket.getPrice();
                    planResponse.totalPrice += ticketResponse.price;

                    for (int i = 0; i < transitStages.size(); i++) {
                        ticketResponse.coverage.add(ticket.getCoverage().get(i));
                    }

                    planResponse.tickets.add(ticketResponse);
                }
            }

            ctx.json(planResponse);
        } catch (Exception e) {
            PlanResponse planResponse = new PlanResponse();

            StringWriter writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));

            planResponse.error = writer.toString();

            ctx.json(planResponse);
            e.printStackTrace();
        }
    }


}
