package ch.ethz.matsim.ch_pt_utils.routing.router;

public interface RouterFactory {
	Router createRouter();
}
